package com.risac.webshop.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockMultipartFile;

import com.risac.webshop.entities.comic.Comic;
import com.risac.webshop.entities.comic.ComicDTO;

@SpringBootTest
public class ControllerHelperTest {
	@Autowired
	ApplicationContext appContext;

	private ComicDTO testItemDTO;

	@BeforeEach
	/**
	 * Creates new test itemDTO for testing purposes.
	 */
	private void setup() throws IOException {

		ComicDTO comicDTO = new ComicDTO();

		comicDTO.setName("Test Name");
		comicDTO.setTitle("Test Title");

		Date testDate = new Date(System.currentTimeMillis());
		comicDTO.setPublishDate(testDate);

		comicDTO.setArtists("Test Artists");
		comicDTO.setWriters("Test Writers");
		comicDTO.setEdition("Test Edition");
		comicDTO.setSerial("Test Serial");
		comicDTO.setIssue(13);
		comicDTO.setOriginalTitle("Test Original Title");
		comicDTO.setOriginalIssue("12");
		comicDTO.setNumPages(98);
		comicDTO.setHardcover(true);
		comicDTO.setColor(false);
		comicDTO.setPrice(300.00);
		comicDTO.setPublisher("Test Publisher");

		InputStream testImage = new FileInputStream(new File("src/test/resources/testImage.jpg"));
		MockMultipartFile file = new MockMultipartFile("File", "TestFileName", null, testImage);
		comicDTO.setTitlePage(file);

		testItemDTO = comicDTO;

	}

	@Test
	public void mapToItemTest() {
		ControllerHelper helper = appContext.getBean(ControllerHelper.class);

		Comic testItem = helper.mapToItem(testItemDTO);

		assertEquals("Test Name", testItem.getName());
		assertEquals("Test Title", testItem.getTitle());
		assertEquals(testItemDTO.getPublishDate(), testItem.getPublishDate());
		assertEquals("Test Artists", testItem.getArtistsString());
		assertEquals("Test Writers", testItem.getWritersString());
		assertEquals("Test Edition", testItem.getEdition());
		assertEquals("Test Serial", testItem.getSerial());
		assertEquals(13, testItem.getIssue());
		assertEquals("Test Original Title", testItem.getOriginalTitle());
		assertEquals(98, testItem.getNumPages());
		assertEquals(true, testItem.isHardcover());
		assertEquals(false, testItem.isColor());
		assertEquals(300.00, testItem.getPrice());
		assertEquals("Test Publisher", testItem.getPublisher().getName());
		assertEquals("/pics/titlepages/TestFileName", testItem.getTitlePagePath());

	}

	@Test
	public void mapToItemFailsIfImageNull() throws IOException, FileNotFoundException {

		InputStream testImage = null;
		MockMultipartFile file = new MockMultipartFile("File", "TestFileName", null, testImage);
		testItemDTO.setTitlePage(file);

		ControllerHelper helper = appContext.getBean(ControllerHelper.class);

		try {
			helper.mapToItem(testItemDTO);
		} catch (IllegalArgumentException e) {
			assertEquals("image == null!", e.getMessage());
		}
	}

}
