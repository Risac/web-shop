package com.risac.webshop;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EntityScan(basePackages = { "com.risac.webshop", "com.risac.webshop." })

public class WebShopV01Application {

	public static void main(String[] args) {
		SpringApplication.run(WebShopV01Application.class, args);
	}

	@Bean
	public CommandLineRunner demo(WebShopApp app) {
		return (args) -> {
			app.onStartup();
		};
	}

}
