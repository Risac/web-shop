package com.risac.webshop.repositories;

import com.risac.webshop.entities.NamedEntity;

public interface BaseEntityRepository<T extends NamedEntity, ID> extends PersistedEntityRepository<T, ID> {

}
