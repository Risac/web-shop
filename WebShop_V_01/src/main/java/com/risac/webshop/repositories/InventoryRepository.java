package com.risac.webshop.repositories;

import com.risac.webshop.entities.inventory.Inventory;

public interface InventoryRepository extends PersistedEntityRepository<Inventory, Long> {

}
