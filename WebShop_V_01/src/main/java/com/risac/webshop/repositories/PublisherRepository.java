package com.risac.webshop.repositories;

import com.risac.webshop.entities.comic.Publisher;

public interface PublisherRepository extends PersistedEntityRepository<Publisher, Long> {

}
