package com.risac.webshop.repositories;

import com.risac.webshop.entities.user.User;

public interface UserRepository extends BaseEntityRepository<User, Long> {

}
