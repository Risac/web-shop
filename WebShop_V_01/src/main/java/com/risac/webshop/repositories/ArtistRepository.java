package com.risac.webshop.repositories;

import com.risac.webshop.entities.comic.Artist;

public interface ArtistRepository extends PersistedEntityRepository<Artist, Long> {

}
