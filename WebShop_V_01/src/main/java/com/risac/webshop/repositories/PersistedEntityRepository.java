package com.risac.webshop.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.risac.webshop.entities.PersistedEntity;

public interface PersistedEntityRepository<T extends PersistedEntity, ID> extends JpaRepository<T, ID> {

}
