package com.risac.webshop.repositories;

import com.risac.webshop.entities.comic.Writer;

public interface WriterRepository extends PersistedEntityRepository<Writer, Long> {

}
