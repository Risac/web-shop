package com.risac.webshop.repositories;

import com.risac.webshop.entities.comic.Comic;

public interface ItemRepository extends BaseEntityRepository<Comic, Long> {

}
