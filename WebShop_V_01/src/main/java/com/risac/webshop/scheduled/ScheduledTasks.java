package com.risac.webshop.scheduled;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.risac.webshop.WebShopApp;
import com.risac.webshop.JPAservices.UserService;
import com.risac.webshop.entities.user.User;

/**
 * ScheduledTasks class provides methods for repeatable tasks (e.g.
 * maintenance).
 */
@Component
public class ScheduledTasks {

	@Autowired
	private UserService userService;

	private final Logger logger = LogManager.getLogger(WebShopApp.class.getName());

	/**
	 * oldUsersCleanup deletes all users created 7 years ago, and deletes all guest
	 * users (unlogged users) 3 days after their session expires.
	 */
	public void oldUsersCleanup() {

		Runnable cleanup = () -> {
			logger.log(Level.INFO, "Users Cleaned Up");
			List<User> findAll = userService.findAll();

//TODO GDPR REQUIREMENT log user last activity, implement last login
			if (findAll != null) {
				for (User user : findAll) {
					// delete user with name saved as SESSION= && created 3 days ago
					if (user.getName().contains("SESSION=")
							&& user.getCreationTime().isBefore(LocalDateTime.now().minusDays(3))) {

						userService.delete(user);
						logger.log(Level.INFO, "User" + user.getName() + "deleted");

					}

					// delete user created 7 years ago
					else if (user.getCreationTime().isBefore(LocalDateTime.now().minusYears(7))) {
						userService.delete(user);
						logger.log(Level.INFO, "User" + user.getName() + "deleted");
					}
				}
			}

		};

		// Schedule cleanup each hour
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
		executor.scheduleWithFixedDelay(cleanup, 0, 1, TimeUnit.DAYS);

	}
}
