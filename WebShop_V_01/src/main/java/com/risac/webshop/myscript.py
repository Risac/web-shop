import csv
import os
import requests
import sys
import time
import json

# Function to make GET request
def make_request_and_save(MPIID, username, password, save_location, base_url):
    url = f"http://{base_url}/csp/healthshare/hsods/fhir/r4/Patient/{MPIID}/$everything"

    # Specify username and password for basic authentication
    auth = (username, password)

    # Make GET request with basic authentication
    response = requests.get(url, auth=auth)

    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        print(f"GET request successful for MPIID: {MPIID}")

        # Save response as JSON
        filename = os.path.join(save_location, f"response_{MPIID}.json")
        with open(filename, 'w') as f:
            json.dump(response.json(), f)

        print(f"Response saved to {filename}")
    else:
        print(f"Failed to make GET request for MPIID: {MPIID}. Status code: {response.status_code}")


# Check if the base URL is provided as a command-line argument
if len(sys.argv) != 2:
    print("Usage: python3 myscript.py <base_url>")
    exit()

base_url = sys.argv[1]

# Replace 'your_file.csv' with the path to your CSV file
file_path = '/home/damir/Downloads/sheet1.csv'

# Specify your username and password for basic authentication
username = '_system'
password = 'vector666'

# Specify the location where you want to save the files
save_location = '/home/damir/Downloads/temp/'

# Open the CSV file
with open(file_path, 'r') as csv_file:
    # Create a CSV reader object
    csv_reader = csv.reader(csv_file)

    # Iterate over each row in the CSV file
    for row in csv_reader:
        # Iterate over each MPIID in the row
        for MPIID in row:
            # Introduce a 5-second delay
            time.sleep(5)

            # Make GET request for each MPIID and save response to file
            make_request_and_save(MPIID, username, password, save_location, base_url)
