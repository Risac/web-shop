package com.risac.webshop;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.risac.webshop.JPAservices.ArtistService;
import com.risac.webshop.JPAservices.InventoryService;
import com.risac.webshop.JPAservices.ItemService;
import com.risac.webshop.JPAservices.PublisherService;
import com.risac.webshop.JPAservices.WriterService;
import com.risac.webshop.controllers.ControllerHelper;
import com.risac.webshop.entities.comic.Artist;
import com.risac.webshop.entities.comic.Comic;
import com.risac.webshop.entities.comic.Publisher;
import com.risac.webshop.entities.comic.Writer;
import com.risac.webshop.entities.inventory.Inventory;
import com.risac.webshop.scheduled.ScheduledTasks;

@Component
@Service
public class WebShopApp {

	@Autowired
	private InventoryService inventoryService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private PublisherService publisherService;

	@Autowired
	private ArtistService artistService;

	@Autowired
	private WriterService writerService;

	@Autowired
	private ControllerHelper helper;

	@Autowired
	private ScheduledTasks tasks;

	private static final Logger logger = LogManager.getLogger(WebShopApp.class.getName());

	/**
	 * Runs on server startup
	 */
	public void onStartup() throws ParseException {
		loadMainInventory();
		scheduledTask();

		// FOR TEST PURPOSES, load test inventory from CSV file
		// loadTestInventory();
	}

	/**
	 * Method loads a small sample of items, assigns them random quantity and saves
	 * them to database.
	 */
	private void loadTestInventory() throws ParseException {

		String line = "";
		String splitBy = ",";

		try {
			// Parsing a CSV file into BufferedReader class constructor
			// Read from file
			BufferedReader br = new BufferedReader(
					new FileReader("src/main/resources/static/item_list/Itemlist.csv", StandardCharsets.UTF_8));

			while ((line = br.readLine()) != null) {
				String[] itemData = line.split(splitBy); // use comma as separator
				Comic comic = new Comic();

				// Start setting values for Item
				comic.setName(itemData[0]);

				Publisher findByName = publisherService.findByName(itemData[1]);
				if (findByName != null) {
					comic.setPublisher(findByName);
				} else {
					Publisher publisher = new Publisher(itemData[1]);
					publisherService.save(publisher);
					comic.setPublisher(publisher);
				}

				String sDate1 = itemData[2];
				Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
				comic.setPublishDate(date1);
				comic.setEdition(itemData[3]);
				comic.setSerial(itemData[4]);
				comic.setIssue(Integer.parseInt(itemData[5]));
				comic.setTitle(itemData[6]);
				comic.setOriginalTitle(itemData[7]);
				comic.setOriginalIssue(itemData[8]);

				List<Artist> art = new ArrayList<>();
				String artistsDTO = itemData[9];
				String[] splitArtists = artistsDTO.split("\\|");

				for (String individualArtist : splitArtists) {
					Artist artist = artistService.findByName(individualArtist);
					if (artist == null) {
						Artist newartist = new Artist(individualArtist);
						artistService.save(newartist);
						art.add(newartist);
					} else {
						art.add(artist);
					}
				}
				comic.setArtists(art);

				List<Writer> writ = new ArrayList<>();
				String writerDTO = itemData[10];
				String[] splitWriters = writerDTO.split("\\|");
				for (String individualWriter : splitWriters) {
					Writer writer = writerService.findByName(individualWriter);
					if (writer == null) {
						Writer newWriter = new Writer(individualWriter);
						writerService.save(newWriter);
						writ.add(newWriter);
					} else {
						writ.add(writer);
					}
				}
				comic.setWriters(writ);

				comic.setNumPages(Integer.parseInt(itemData[11]));
				comic.setPrice(Double.parseDouble(itemData[12]));
				comic.setColor(false);
				if (itemData[13].equals("TRUE")) {
					comic.setColor(true);
				}
				comic.setHardcover(false);
				if (itemData[14].equals("TRUE")) {
					comic.setHardcover(true);
				}

				comic.setTitlePagePath(itemData[15]);

				// save stock
				Random rand = new Random();
				int rand_int1 = rand.nextInt(10);
				comic.setStockLevel(rand_int1);
				itemService.save(comic);

				helper.updateInventory(comic);

			}

		} catch (

		IOException e) {
			e.printStackTrace();
		}
	}

	private void loadMainInventory() {
		Inventory inventory = inventoryService.findByName("Main inventory");

		if (inventory == null) {
			List<Comic> stockList = new ArrayList<>();
			inventory = new Inventory("Main inventory", stockList);
			inventoryService.save(inventory);

		}

		else {
			logger.log(Level.INFO, "Main inventory loaded");

		}

	}

	private void scheduledTask() {

		tasks.oldUsersCleanup();

	}
}
