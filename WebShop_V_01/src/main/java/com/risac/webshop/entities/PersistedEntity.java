package com.risac.webshop.entities;

import java.util.Objects;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * PersistedEntity holds parameter: id.
 */
@MappedSuperclass
public class PersistedEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	public PersistedEntity() {
	}

	public PersistedEntity(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PersistedEntity other = (PersistedEntity) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "PersistedEntity [id=" + id + "]";
	}

}
