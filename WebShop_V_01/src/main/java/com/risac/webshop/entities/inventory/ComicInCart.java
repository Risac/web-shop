package com.risac.webshop.entities.inventory;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.risac.webshop.entities.PersistedEntity;
import com.risac.webshop.entities.comic.Comic;

/**
 * Entity ItemInCart holds parameters: item and quantity, used for a list of
 * items in user's cart.
 */
@Entity
public class ComicInCart extends PersistedEntity {

	@ManyToOne
	private Comic comic;

	private Integer quantity;

	public ComicInCart() {
	}

	public ComicInCart(Comic comic, Integer quantity) {
		super();
		this.comic = comic;
		this.quantity = quantity;
	}

	public Comic getItem() {
		return comic;
	}

	public void setItem(Comic comic) {
		this.comic = comic;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		// int result = super.hashCode();
		int result = prime + Objects.hash(comic, quantity);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ComicInCart other = (ComicInCart) obj;
		return Objects.equals(comic, other.comic) && Objects.equals(quantity, other.quantity);
	}

	@Override
	public String toString() {
		return "Cart [item=" + comic + ", quantity=" + quantity + "]" + "id= " + getId();
	}

}
