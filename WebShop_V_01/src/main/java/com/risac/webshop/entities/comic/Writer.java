package com.risac.webshop.entities.comic;

import javax.persistence.Entity;

import com.risac.webshop.entities.NamedEntity;

/**
 * Entity Writer holds parameter: Writer for List<Writer>.
 */
@Entity
public class Writer extends NamedEntity {

	public Writer() {
		super();
	}

	public Writer(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return getName();
	}

}
