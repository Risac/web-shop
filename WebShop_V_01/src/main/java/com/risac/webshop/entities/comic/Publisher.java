package com.risac.webshop.entities.comic;

import javax.persistence.Entity;

import com.risac.webshop.entities.NamedEntity;

/**
 * Publisher holds parameter: Publisher.
 */
@Entity
public class Publisher extends NamedEntity {

	public Publisher() {
	}

	public Publisher(String name) {
		super(name);
	}

}
