package com.risac.webshop.entities.user;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;

import com.risac.webshop.entities.NamedEntity;
import com.risac.webshop.entities.comic.Comic;

/**
 * User is entity saved to database.
 */
@Entity
public class User extends NamedEntity {

	private String f_name;
	private String l_name;
	private String phone;
	private String address;
	private String password;
	private int zip;
	private String city;
	private String email;

	// @ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	@ElementCollection
	private Map<Comic, Integer> cart;
	private LocalDateTime creationTime;

	public User() {
	}

	public User(String name, String f_name, String l_name, String phone, String address, String password, int zip,
			String city, String email, Map<Comic, Integer> cart, LocalDateTime creationTime) {
		super(name);
		this.f_name = f_name;
		this.l_name = l_name;
		this.phone = phone;
		this.address = address;
		this.password = password;
		this.zip = zip;
		this.city = city;
		this.email = email;
		this.cart = cart;
		this.creationTime = creationTime;
	}

	public String getF_name() {
		return f_name;
	}

	public void setF_name(String f_name) {
		this.f_name = f_name;
	}

	public String getL_name() {
		return l_name;
	}

	public void setL_name(String l_name) {
		this.l_name = l_name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getZip() {
		return zip;
	}

	public void setZip(int zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Map<Comic, Integer> getCart() {
		return cart;
	}

	public void setCart(Map<Comic, Integer> cart) {
		this.cart = cart;
	}

	public LocalDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(LocalDateTime creationTime) {
		this.creationTime = creationTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ Objects.hash(address, cart, city, creationTime, email, f_name, l_name, password, phone, zip);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		User other = (User) obj;
		return Objects.equals(address, other.address) && Objects.equals(cart, other.cart)
				&& Objects.equals(city, other.city) && Objects.equals(creationTime, other.creationTime)
				&& Objects.equals(email, other.email) && Objects.equals(f_name, other.f_name)
				&& Objects.equals(l_name, other.l_name) && Objects.equals(password, other.password)
				&& Objects.equals(phone, other.phone) && zip == other.zip;
	}

	@Override
	public String toString() {
		return "User [f_name=" + f_name + ", l_name=" + l_name + ", phone=" + phone + ", address=" + address
				+ ", password=" + password + ", zip=" + zip + ", city=" + city + ", email=" + email + ", cart=" + cart
				+ ", creationTime=" + creationTime + "]";
	}

}
