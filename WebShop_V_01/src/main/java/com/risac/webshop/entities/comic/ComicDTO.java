package com.risac.webshop.entities.comic;

import java.util.Date;
import java.util.Objects;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Transient;
import org.springframework.web.multipart.MultipartFile;

import com.risac.webshop.entities.NamedEntity;

/**
 * Entity ComicDTO is used to transfer data to entity Comic.
 */
@Transient
public class ComicDTO extends NamedEntity {

	private String publisher;

	@DateTimeFormat(pattern = "yyyy")
	private Date publishDate;

	private String edition;
	private String serial;
	private Integer issue;

	private String artists;

	private String writers;
	private String title;
	private String originalTitle;
	private String originalIssue;
	private Integer numPages;
	private Boolean color;
	private Boolean hardcover;
	private Double price;
	private MultipartFile titlePage;
	private String command;
	private Integer stockLevel;

	public ComicDTO(String name, String publisher, Date publishDate, String edition, String serial, Integer issue,
			String artists, String writers, String title, String originalTitle, String originalIssue, Integer numPages,
			Boolean color, Boolean hardcover, Double price, MultipartFile titlePage, String command,
			Integer stockLevel) {
		super(name);
		this.publisher = publisher;
		this.publishDate = publishDate;
		this.edition = edition;
		this.serial = serial;
		this.issue = issue;
		this.artists = artists;
		this.writers = writers;
		this.title = title;
		this.originalTitle = originalTitle;
		this.originalIssue = originalIssue;
		this.numPages = numPages;
		this.color = color;
		this.hardcover = hardcover;
		this.price = price;
		this.titlePage = titlePage;
		this.command = command;
		this.stockLevel = stockLevel;
	}

	public ComicDTO() {
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public Integer getIssue() {
		return issue;
	}

	public void setIssue(Integer issue) {
		this.issue = issue;
	}

	public String getArtists() {
		return artists;
	}

	public void setArtists(String artists) {
		this.artists = artists;
	}

	public String getWriters() {
		return writers;
	}

	public void setWriters(String writers) {
		this.writers = writers;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public String getOriginalIssue() {
		return originalIssue;
	}

	public void setOriginalIssue(String originalIssue) {
		this.originalIssue = originalIssue;
	}

	public Integer getNumPages() {
		return numPages;
	}

	public void setNumPages(Integer numPages) {
		this.numPages = numPages;
	}

	public Boolean getColor() {
		return color;
	}

	public void setColor(Boolean color) {
		this.color = color;
	}

	public Boolean getHardcover() {
		return hardcover;
	}

	public void setHardcover(Boolean hardcover) {
		this.hardcover = hardcover;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public MultipartFile getTitlePage() {
		return titlePage;
	}

	public void setTitlePage(MultipartFile titlePage) {
		this.titlePage = titlePage;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Integer getStockLevel() {
		return stockLevel;
	}

	public void setStockLevel(Integer stockLevel) {
		this.stockLevel = stockLevel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(artists, color, command, edition, hardcover, issue, numPages,
				originalIssue, originalTitle, price, publishDate, publisher, serial, stockLevel, title, writers);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ComicDTO other = (ComicDTO) obj;
		return Objects.equals(artists, other.artists) && Objects.equals(color, other.color)
				&& Objects.equals(command, other.command) && Objects.equals(edition, other.edition)
				&& Objects.equals(hardcover, other.hardcover) && Objects.equals(issue, other.issue)
				&& Objects.equals(numPages, other.numPages) && Objects.equals(originalIssue, other.originalIssue)
				&& Objects.equals(originalTitle, other.originalTitle) && Objects.equals(price, other.price)
				&& Objects.equals(publishDate, other.publishDate) && Objects.equals(publisher, other.publisher)
				&& Objects.equals(serial, other.serial) && Objects.equals(stockLevel, other.stockLevel)
				&& Objects.equals(title, other.title) && Objects.equals(writers, other.writers);
	}

	@Override
	public String toString() {
		return "ItemDTO [publisher=" + publisher + ", publishDate=" + publishDate + ", edition=" + edition + ", serial="
				+ serial + ", issue=" + issue + ", artists=" + artists + ", writers=" + writers + ", title=" + title
				+ ", originalTitle=" + originalTitle + ", originalIssue=" + originalIssue + ", numPages=" + numPages
				+ ", color=" + color + ", hardcover=" + hardcover + ", price=" + price + ", titlePage=" + titlePage
				+ ", command=" + command + ", stockLevel=" + stockLevel + "]";
	}

}
