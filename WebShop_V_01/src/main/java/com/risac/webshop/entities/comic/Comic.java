package com.risac.webshop.entities.comic;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.risac.webshop.entities.NamedEntity;

/**
 * Entity Comic holds data representing a single comic.
 */
@Entity
public class Comic extends NamedEntity {

	@ManyToOne
	private Publisher publisher;

	@DateTimeFormat(pattern = "yyyy")
	private Date publishDate;

	private String edition;
	private String serial;
	private int issue;

	@ManyToMany
	private List<Artist> artists;

	@ManyToMany
	private List<Writer> writers;

	private String title;
	private String originalTitle;
	private String originalIssue;
	private int numPages;
	private boolean color;
	private boolean hardcover;
	private double price;
	private String titlePagePath;
	private int stockLevel;

	public Comic() {
	}

	public Comic(Publisher publisher, Date publishDate, String edition, String serial, int issue, List<Artist> artists,
			List<Writer> writers, String title, String originalTitle, String originalIssue, int numPages, boolean color,
			boolean hardcover, double price, String titlePagePath, int stockLevel) {
		super();
		this.publisher = publisher;
		this.publishDate = publishDate;
		this.edition = edition;
		this.serial = serial;
		this.issue = issue;
		this.artists = artists;
		this.writers = writers;
		this.title = title;
		this.originalTitle = originalTitle;
		this.originalIssue = originalIssue;
		this.numPages = numPages;
		this.color = color;
		this.hardcover = hardcover;
		this.price = price;
		this.titlePagePath = titlePagePath;
		this.stockLevel = stockLevel;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public int getIssue() {
		return issue;
	}

	public void setIssue(int issue) {
		this.issue = issue;
	}

	public List<Artist> getArtists() {
		return artists;
	}

	public void setArtists(List<Artist> artists) {
		this.artists = artists;
	}

	public List<Writer> getWriters() {
		return writers;
	}

	public void setWriters(List<Writer> writers) {
		this.writers = writers;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public String getOriginalIssue() {
		return originalIssue;
	}

	public void setOriginalIssue(String originalIssue) {
		this.originalIssue = originalIssue;
	}

	public int getNumPages() {
		return numPages;
	}

	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}

	public boolean isColor() {
		return color;
	}

	public void setColor(boolean color) {
		this.color = color;
	}

	public boolean isHardcover() {
		return hardcover;
	}

	public void setHardcover(boolean hardcover) {
		this.hardcover = hardcover;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getTitlePagePath() {
		return titlePagePath;
	}

	public void setTitlePagePath(String titlePagePath) {
		this.titlePagePath = titlePagePath;
	}

	public int getStockLevel() {
		return stockLevel;
	}

	public void setStockLevel(int stockLevel) {
		this.stockLevel = stockLevel;
	}

	public String getPriceString() {
		return String.format("%.2f", price);
	}

	public String getArtistsString() {
		String artistList = Arrays.toString(artists.toArray());
		return artistList.substring(1, artistList.length() - 1);
	}

	public String getWritersString() {
		String writerList = Arrays.toString(writers.toArray());
		return writerList.substring(1, writerList.length() - 1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(artists, color, edition, hardcover, issue, numPages, originalIssue,
				originalTitle, price, publishDate, publisher, serial, stockLevel, title, titlePagePath, writers);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Comic other = (Comic) obj;
		return Objects.equals(artists, other.artists) && color == other.color && Objects.equals(edition, other.edition)
				&& hardcover == other.hardcover && issue == other.issue && numPages == other.numPages
				&& Objects.equals(originalIssue, other.originalIssue)
				&& Objects.equals(originalTitle, other.originalTitle)
				&& Double.doubleToLongBits(price) == Double.doubleToLongBits(other.price)
				&& Objects.equals(publishDate, other.publishDate) && Objects.equals(publisher, other.publisher)
				&& Objects.equals(serial, other.serial) && stockLevel == other.stockLevel
				&& Objects.equals(title, other.title) && Objects.equals(titlePagePath, other.titlePagePath)
				&& Objects.equals(writers, other.writers);
	}

	@Override
	public String toString() {
		return "Comic [getId()=" + getId() + ", getName()=" + getName() + ", publisher=" + publisher + ", publishDate="
				+ publishDate + ", edition=" + edition + ", serial=" + serial + ", issue=" + issue + ", artists="
				+ artists + ", writers=" + writers + ", title=" + title + ", originalTitle=" + originalTitle
				+ ", originalIssue=" + originalIssue + ", numPages=" + numPages + ", color=" + color + ", hardcover="
				+ hardcover + ", price=" + price + ", titlePagePath=" + titlePagePath + ", stockLevel=" + stockLevel
				+ "]";
	}

}
