package com.risac.webshop.entities;

import java.util.Objects;

import javax.persistence.MappedSuperclass;

/**
 * BaseEntity holds parameter: name.
 */
@MappedSuperclass
//TODO rename to NamedEntity
public class NamedEntity extends PersistedEntity {

	private String name;

	public NamedEntity() {
	}

	public NamedEntity(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(name);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NamedEntity other = (NamedEntity) obj;
		return Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "BaseEntity [name=" + name + ", getId()=" + getId() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + "]";
	}

}
