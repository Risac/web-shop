package com.risac.webshop.entities.user;

import java.time.LocalDateTime;
import java.util.Objects;

public class Card {

	private int cardNumber;
	private String nameOnCard;
	private int cvv2;
	private LocalDateTime validDate;

	public Card() {
	}

	public Card(int cardNumber, String nameOnCard, int cvv2, LocalDateTime validDate) {
		super();
		this.cardNumber = cardNumber;
		this.nameOnCard = nameOnCard;
		this.cvv2 = cvv2;
		this.validDate = validDate;
	}

	public int getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(int cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public int getCvv2() {
		return cvv2;
	}

	public void setCvv2(int cvv2) {
		this.cvv2 = cvv2;
	}

	public LocalDateTime getValidDate() {
		return validDate;
	}

	public void setValidDate(LocalDateTime validDate) {
		this.validDate = validDate;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cardNumber, cvv2, nameOnCard, validDate);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Card other = (Card) obj;
		return cardNumber == other.cardNumber && cvv2 == other.cvv2 && Objects.equals(nameOnCard, other.nameOnCard)
				&& Objects.equals(validDate, other.validDate);
	}

	@Override
	public String toString() {
		return "Card [cardNumber=" + cardNumber + ", nameOnCard=" + nameOnCard + ", cvv2=" + cvv2 + ", validDate="
				+ validDate + "]";
	}

}
