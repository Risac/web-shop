package com.risac.webshop.entities.comic;

import javax.persistence.Entity;

import com.risac.webshop.entities.NamedEntity;

/**
 * Artist holds parameter: Artist for List<Artist>.
 */
@Entity
public class Artist extends NamedEntity {

	public Artist() {
		super();
	}

	public Artist(String name) {
		super(name);
	}

	@Override
	public String toString() {

		return getName();
	}

}
