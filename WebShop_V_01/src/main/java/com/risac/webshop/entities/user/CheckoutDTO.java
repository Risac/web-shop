package com.risac.webshop.entities.user;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Transient;

@Transient
public class CheckoutDTO {

	private String f_name;
	private String l_name;
	private String phone;
	private String address;
	private String password;
	private Integer zip;
	private String city;
	private String email;
	private Long cardNumber;
	private String nameOnCard;
	private Integer cvv2;

	@DateTimeFormat(pattern = "yyyy")
	private Date validDate;

	public CheckoutDTO() {
	}

	public CheckoutDTO(String f_name, String l_name, String phone, String address, String password, Integer zip,
			String city, String email, Long cardNumber, String nameOnCard, Integer cvv2, Date validDate) {
		super();
		this.f_name = f_name;
		this.l_name = l_name;
		this.phone = phone;
		this.address = address;
		this.password = password;
		this.zip = zip;
		this.city = city;
		this.email = email;
		this.cardNumber = cardNumber;
		this.nameOnCard = nameOnCard;
		this.cvv2 = cvv2;
		this.validDate = validDate;
	}

	public String getF_name() {
		return f_name;
	}

	public void setF_name(String f_name) {
		this.f_name = f_name;
	}

	public String getL_name() {
		return l_name;
	}

	public void setL_name(String l_name) {
		this.l_name = l_name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getZip() {
		return zip;
	}

	public void setZip(Integer zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(Long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getNameOnCard() {
		return nameOnCard;
	}

	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}

	public Integer getCvv2() {
		return cvv2;
	}

	public void setCvv2(Integer cvv2) {
		this.cvv2 = cvv2;
	}

	public Date getValidDate() {
		return validDate;
	}

	public void setValidDate(Date validDate) {
		this.validDate = validDate;
	}

	@Override
	public String toString() {
		return "CheckoutDTO [f_name=" + f_name + ", l_name=" + l_name + ", phone=" + phone + ", address=" + address
				+ ", password=" + password + ", zip=" + zip + ", city=" + city + ", email=" + email + ", cardNumber="
				+ cardNumber + ", nameOnCard=" + nameOnCard + ", cvv2=" + cvv2 + ", validDate=" + validDate + "]";
	}

}
