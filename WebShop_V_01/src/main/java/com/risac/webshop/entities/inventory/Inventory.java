package com.risac.webshop.entities.inventory;

import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.risac.webshop.entities.NamedEntity;
import com.risac.webshop.entities.comic.Comic;

/**
 * Entity Inventory holds a list of Stock (item+quantity).
 */
@Entity
public class Inventory extends NamedEntity {

	@OneToMany
	private List<Comic> inventoryList;

	public Inventory() {
	}

	public Inventory(String name, List<Comic> inventoryList) {
		super(name);
		this.inventoryList = inventoryList;
	}

	public List<Comic> getInventoryList() {
		return inventoryList;
	}

	public void setInventoryList(List<Comic> inventoryList) {
		this.inventoryList = inventoryList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(inventoryList);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Inventory other = (Inventory) obj;
		return Objects.equals(inventoryList, other.inventoryList);
	}

	@Override
	public String toString() {
		return "Inventory [inventoryList=" + inventoryList + "]";
	}

}
