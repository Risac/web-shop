package com.risac.webshop.exceptions;

public class ItemNotFoundException extends RuntimeException {

	public ItemNotFoundException(Long id) {
		super("Could not find Item with id: " + id);
	}

}
