package com.risac.webshop.JPAservices;

import java.util.List;

import com.risac.webshop.entities.comic.Comic;
import com.risac.webshop.entities.comic.Publisher;
import com.risac.webshop.repositories.ItemRepository;

public interface ItemService extends ItemRepository {

	List<Comic> findByPublisher(Publisher publisher);

	List<Comic> findByNameContaining(String title);

	List<Comic> findByPublisher(String publisher);

}
