package com.risac.webshop.JPAservices;

import com.risac.webshop.entities.comic.Artist;
import com.risac.webshop.repositories.ArtistRepository;

public interface ArtistService extends ArtistRepository {

	Artist findByName(String name);
}
