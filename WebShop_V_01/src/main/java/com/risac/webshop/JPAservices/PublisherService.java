package com.risac.webshop.JPAservices;

import java.util.List;

import com.risac.webshop.entities.comic.Publisher;
import com.risac.webshop.repositories.PublisherRepository;

public interface PublisherService extends PublisherRepository {

	Publisher findByName(String name);

	List<Publisher> findByNameContaining(String name);
}
