package com.risac.webshop.JPAservices;

import com.risac.webshop.entities.user.User;
import com.risac.webshop.repositories.UserRepository;

public interface UserService extends UserRepository {

	User findByName(String username);

	boolean existsByName(String name);

	boolean existsByEmail(String email);

}
