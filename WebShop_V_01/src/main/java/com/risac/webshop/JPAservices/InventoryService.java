package com.risac.webshop.JPAservices;

import com.risac.webshop.entities.inventory.Inventory;
import com.risac.webshop.repositories.InventoryRepository;

public interface InventoryService extends InventoryRepository {

	Inventory findByName(String name);

}
