package com.risac.webshop.JPAservices;

import com.risac.webshop.entities.comic.Writer;
import com.risac.webshop.repositories.WriterRepository;

public interface WriterService extends WriterRepository {

	Writer findByName(String name);
}
