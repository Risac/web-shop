package com.risac.webshop.controllers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.risac.webshop.WebShopApp;
import com.risac.webshop.JPAservices.InventoryService;
import com.risac.webshop.JPAservices.ItemService;
import com.risac.webshop.JPAservices.PublisherService;
import com.risac.webshop.JPAservices.UserService;
import com.risac.webshop.entities.comic.Comic;
import com.risac.webshop.entities.comic.ComicDTO;
import com.risac.webshop.entities.comic.Publisher;
import com.risac.webshop.entities.inventory.ComicInCart;
import com.risac.webshop.entities.user.CheckoutDTO;
import com.risac.webshop.entities.user.User;

import io.micrometer.core.lang.NonNull;

@Controller
public class MainController {

	@Autowired
	private ItemService itemService;
	@Autowired
	private UserService userServ;
//	@Autowired
//	private StockService stockService;
	@Autowired
	UserService userService;
	@Autowired
	InventoryService inventoryService;
	@Autowired
	PublisherService publisherService;

	@Autowired
	private ControllerHelper helper;

	private static final Logger logger = LogManager.getLogger(WebShopApp.class.getName());

	/*
	 * Maps index.html. Parameter itemId used for forwarding to single page item.
	 * Parameter command used for sending data from front-end elements.
	 */
	@GetMapping("/")
	@Transactional
	public ModelAndView index(@RequestParam(value = "itemId", required = false) Integer itemId,
			@RequestParam(required = false) String command, @RequestParam(required = false) Long comicId,
			HttpSession session) {
		ModelAndView modelAndView = new ModelAndView();

		// Find user in current session
		String userName = helper.findLoggedUserName(session);
		User findUser = userServ.findByName(userName);

		if (findUser == null) {
			helper.saveAnonimUser(userName); // Create new anonimous user, assign him a cart and save to database
		}

		Map<Comic, Integer> itemsInCart = helper.getUserCart(userName); // get cart for User
		List<Comic> allItems = itemService.findAll();

		// Clear items from right-hand Cart menu.
		if (command != null) {

			if (command.equals("cancel")) {
				helper.clearCartList(comicId, userName);
				modelAndView = new ModelAndView("redirect:/");
				return modelAndView;
			}

			if (command.equals("forwardMe")) {
				modelAndView.addObject("itemid", itemId);
				modelAndView = new ModelAndView("forward:/view_comic");
				return modelAndView;
			}
			if (command.equals("addMe") && itemId != null) {
				Optional<Comic> findById = itemService.findById((long) itemId);
				Comic comic = findById.get();

				Integer numItems = itemsInCart.get(comic);
				if (numItems == null) {
					numItems = 0;
				}
				itemsInCart.put(comic, numItems + 1);

				// clear command (prevents refreshing page from adding more items)
				modelAndView = new ModelAndView("redirect:/");
				return modelAndView;
			}
		}

		// Search filters
		if (command != null) {
			// Find by title Containing
			List<Comic> findByTitleContaining = itemService.findByNameContaining(command);
			// findByNameLike alternative would be slower.

			// ALTERNATIVE: Not used
//			List<Item> collectedNames = itemService.findAll().stream()
//					.filter(i -> i.getName().toLowerCase().contains(command.toLowerCase()))
//					.collect(Collectors.toList());

			List<Publisher> publisher = publisherService.findByNameContaining(command); // find by Publisher Containing
			List<Comic> findByPublisherName = new ArrayList<>();

			for (Publisher item : publisher) {
				findByPublisherName.addAll(itemService.findByPublisher(item)); // add all Publishers
			}
			allItems = findByTitleContaining;
			allItems.addAll(findByPublisherName); // change list allItems to filtered list
		}
		// Change name for welcome display on page
		if (userName.contains("SESSION=")) {
			userName = "Guest";
		}

		modelAndView.addObject("greetUser", userName);
		modelAndView.addObject("itemsInCart", itemsInCart);
		modelAndView.addObject("allItems", allItems);
		modelAndView.setViewName("index.html");

		return modelAndView;
	}

	/*
	 * Maps view_comic.html
	 */
	@RequestMapping("/view_comic")
	public ModelAndView viewComic(Long itemId, ComicInCart comicInCart, @RequestParam(required = false) String command,
			HttpSession session) {
		ModelAndView modelAndView = new ModelAndView();

		// Find user in current session
		String userName = helper.findLoggedUserName(session);
		User findUser = userServ.findByName(userName);

		Map<Comic, Integer> itemsInCart = helper.getUserCart(userName);
		Comic comic = null;
		// test quantity
		if (itemId != null) {
			Optional<Comic> findById = itemService.findById(itemId);
			comic = findById.get();
			// get stock level
			// Stock findByItem = stockService.findByItem(comic);
			// System.err.println(this.getClass() + " stockLevel: " +
			// findByItem.getQuantity());
		}

		// Add item to cart
		if (command != null) {
			if (command.equals("addMe")) {
				comicInCart.setItem(comic);

				if (comicInCart.getQuantity() == null) { // set default value of quantity to 1
					comicInCart.setQuantity(1);
				}

				itemsInCart.put(comicInCart.getItem(), comicInCart.getQuantity());
			}
		}

		if (findUser != null) {
			findUser.setCart(itemsInCart);
			userServ.save(findUser);
		}

		// TODO
		// Use Optional.erElse()
		Optional<Comic> optionalItem = itemService.findById(itemId); // find by forwarded Id
		comic = optionalItem.get(); // Optional to Item

		if (userName.contains("SESSION=")) {
			userName = "Guest";
		}
		modelAndView.addObject("greetUser", userName);
		modelAndView.addObject("itemsInCart", itemsInCart);
		modelAndView.addObject("cart", comicInCart);
		modelAndView.addObject("item", comic);
		modelAndView.setViewName("view_comic.html");

		return modelAndView;
	}

	/*
	 * Maps cart.html
	 */
	@RequestMapping("/cart")
	@Transactional
	public ModelAndView showCart(ComicInCart comicInCart, @RequestParam(required = false) String command,
			HttpSession session) {
		ModelAndView modelAndView = new ModelAndView();

		Double total = 0.00;

		// Find user in current session
		String userName = helper.findLoggedUserName(session);

		total = helper.calculateTotal(comicInCart, userName);

		Map<Comic, Integer> itemsInCart = helper.getUserCart(userName);

		if (command != null) {
			if (command.equals("cancel")) {
				itemsInCart.clear();
				total = 0.00;

			} else if (command.equals("checkout")) {
				modelAndView = new ModelAndView("redirect:/checkout");
				return modelAndView;
			}
		}

		modelAndView.addObject("itemsInCart", itemsInCart);
		modelAndView.addObject("total", total);
		modelAndView.setViewName("cart.html");

		return modelAndView;
	}

	/*
	 * Maps checkout.html
	 */
	@RequestMapping("/checkout")
	public ModelAndView checkout(@ModelAttribute("checkoutDTO") CheckoutDTO checkoutDTO, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView();

		// Find user in current session
		String userName = helper.findLoggedUserName(session);
		User findUser = userServ.findByName(userName);
		Map<Comic, Integer> itemsInCart = helper.getUserCart(userName);

		if (checkoutDTO != null) {
			System.err.println(checkoutDTO);
		}

		if (userName.contains("SESSION=")) {
			userName = "Guest";
		}

		modelAndView.addObject("greetUser", userName);
		modelAndView.addObject("findUser", findUser);
		modelAndView.setViewName("checkout.html");
		return modelAndView;
	}

	/*
	 * Maps add_item.html
	 */
	@RequestMapping(value = "/admin_add_item")
	public ModelAndView addItem(@ModelAttribute("itemDTO") @NonNull ComicDTO comicDTO, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView();

		if (comicDTO.getName() != null && comicDTO.getTitlePage() != null) {
			Comic newItem = helper.mapToItem(comicDTO); // method maps DTO to Item
			itemService.save(newItem);

			// save stock
			// Stock stock = new Stock(newItem, comicDTO.getStockLevel());
			// stockService.save(stock);

			helper.updateInventory(newItem);
			logger.log(Level.INFO, "Item: " + newItem.getName() + " added with Quantity: " + newItem.getStockLevel());

			modelAndView = new ModelAndView("redirect:/");
			return modelAndView; // return to index.html
		}

		// Find user in current session
		String userName = helper.findLoggedUserName(session);
		Map<Comic, Integer> itemsInCart = helper.getUserCart(userName);

		modelAndView.addObject("itemsInCart", itemsInCart);
		modelAndView.setViewName("admin_add_item.html");
		return modelAndView;
	}

	/*
	 * Maps admin_check_inventory.html
	 */
	@RequestMapping("/admin_check_inventory")
	@Transactional
	public ModelAndView checkInventory(@RequestParam(required = false) String command) {
		ModelAndView modelAndView = new ModelAndView();
		List<Comic> stock = inventoryService.findByName("Main Inventory").getInventoryList();

		if (command != null) {
			if (command.equals("sortId")) {
				Comparator<Comic> sortById = (s1, s2) -> s1.getId().compareTo(s2.getId());
				stock.sort(sortById);
			}

			if (command.equals("sortQuantity")) {
				Comparator<Comic> sortByQuantity = (s1, s2) -> s1.getStockLevel() - (s2.getStockLevel());
				stock.sort(sortByQuantity);

			}
			if (command.equals("sortPublisher")) {
				Comparator<Comic> sortByPublisher = (s1, s2) -> s1.getPublisher().getName()
						.compareTo(s2.getPublisher().getName());
				stock.sort(sortByPublisher);
			}

			if (command.equals("sortDate")) {
				Comparator<Comic> sortByDate = (s1, s2) -> s1.getPublishDate().compareTo(s2.getPublishDate());
				stock.sort(sortByDate);
			}
		}
		modelAndView.addObject("stock", stock);
		modelAndView.setViewName("admin_check_inventory.html");
		return modelAndView;
	}

	/*
	 * Maps register.html
	 */
	@RequestMapping("/register")
	public ModelAndView registerUser(@ModelAttribute("user") User user, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView();

		if (user.getName() != null) {
			// Encode password
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String encodedPassword = passwordEncoder.encode(user.getPassword());
			user.setPassword(encodedPassword);
			user.setCreationTime(LocalDateTime.now());

			boolean existsByName = userServ.existsByName(user.getName());
			boolean existsByEmail = userServ.existsByEmail(user.getEmail());
			if (existsByName || existsByEmail) {
				modelAndView = new ModelAndView("redirect:/error_user");
				return modelAndView;

			} else {
				userServ.save(user);
				logger.log(Level.INFO, "User: " + user.getName() + " saved.");
			}

			modelAndView = new ModelAndView("redirect:/login");
			return modelAndView;

		}

		// Find user in current session
		String userName = helper.findLoggedUserName(session);
		Map<Comic, Integer> itemsInCart = helper.getUserCart(userName);

		modelAndView.addObject("itemsInCart", itemsInCart);
		modelAndView.setViewName("register.html");
		return modelAndView;
	}

	/*
	 * Maps admin_check_inventory.html
	 */
	@RequestMapping("/error_user")

	public ModelAndView errorUser() {
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.setViewName("error_user.html");
		return modelAndView;
	}
}
