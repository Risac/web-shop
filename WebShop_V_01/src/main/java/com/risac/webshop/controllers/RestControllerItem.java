package com.risac.webshop.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.risac.webshop.JPAservices.ItemService;
import com.risac.webshop.entities.comic.Comic;
import com.risac.webshop.exceptions.ItemNotFoundException;

@RestController
public class RestControllerItem {

	private final ItemService itemService;

	RestControllerItem(ItemService itemService) {
		this.itemService = itemService;
	}

	@GetMapping("/items")
	List<Comic> all() {
		return itemService.findAll();
	}

	// Find single Item by Id
	@GetMapping("/items/{id}")
	Comic one(@PathVariable Long id) {
		return itemService.findById(id).orElseThrow(() -> new ItemNotFoundException(id));
	}

}
