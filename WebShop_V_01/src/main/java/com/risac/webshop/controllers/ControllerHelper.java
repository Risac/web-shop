package com.risac.webshop.controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.risac.webshop.WebShopApp;
import com.risac.webshop.JPAservices.ArtistService;
import com.risac.webshop.JPAservices.InventoryService;
import com.risac.webshop.JPAservices.ItemService;
import com.risac.webshop.JPAservices.PublisherService;
import com.risac.webshop.JPAservices.UserService;
import com.risac.webshop.JPAservices.WriterService;
import com.risac.webshop.entities.comic.Artist;
import com.risac.webshop.entities.comic.Comic;
import com.risac.webshop.entities.comic.ComicDTO;
import com.risac.webshop.entities.comic.Publisher;
import com.risac.webshop.entities.comic.Writer;
import com.risac.webshop.entities.inventory.Inventory;
import com.risac.webshop.entities.inventory.ComicInCart;
import com.risac.webshop.entities.user.User;

@Service
public class ControllerHelper {

	@Autowired
	private PublisherService publisherService;

	@Autowired
	private ArtistService artistService;

	@Autowired
	private WriterService writerService;

	@Autowired
	private InventoryService inventoryService;

	@Autowired
	private UserService userService;

	@Autowired
	private ItemService itemService;

	private static final Logger logger = LogManager.getLogger(WebShopApp.class.getName());

	/**
	 * Methods maps provided itemDTO to Item model
	 */
	public Comic mapToItem(ComicDTO comicDTO) {
		Comic comic = new Comic();

		// Finding and setting publiser
		Publisher findByName = publisherService.findByName(comicDTO.getPublisher());
		if (findByName != null) {
			comic.setPublisher(findByName); // set publisher
		} else {
			Publisher publisher = new Publisher(comicDTO.getPublisher()); // create new Publisher, save and set it
			publisherService.save(publisher);
			comic.setPublisher(publisher);
		}

		// Finding and setting artists
		List<Artist> art = new ArrayList<>();
		String artistsDTO = comicDTO.getArtists(); // get artist as String
		String[] splitArtists = artistsDTO.split(", "); // split to array

		for (String individualArtist : splitArtists) { // loop through array of artist
			Artist artist = artistService.findByName(individualArtist); // find artist by name
			if (artist == null) {
				Artist newartist = new Artist(individualArtist); // create new Artist, save it
				artistService.save(newartist);
				art.add(newartist);

			} else {
				art.add(artist);
			}

		}
		comic.setArtists(art); // set Artist list

		// Finding and setting writers
		List<Writer> writ = new ArrayList<>();
		String writerDTO = comicDTO.getWriters(); // get writers as String
		String[] splitWriters = writerDTO.split(", "); // split to array

		for (String individualWriter : splitWriters) { // loop through array of artist
			Writer writer = writerService.findByName(individualWriter); // find writer by name
			if (writer == null) {
				Writer newWriter = new Writer(individualWriter); // create new Writer, save it
				writerService.save(newWriter);
				writ.add(newWriter);
			} else {
				writ.add(writer);
			}
		}
		comic.setWriters(writ); // set Writers list

		// Title Page File upload
		processFile(comicDTO);

		String originalFilename = comicDTO.getTitlePage().getOriginalFilename();
		String titlePagePath = "/pics/titlepages/" + originalFilename;
		comic.setTitlePagePath(titlePagePath);

		// The rest are standard setters from itemDTO
		comic.setName(comicDTO.getName());
		comic.setTitle(comicDTO.getTitle());
		comic.setPublishDate(comicDTO.getPublishDate());
		comic.setEdition(comicDTO.getEdition());
		comic.setSerial(comicDTO.getSerial());
		comic.setIssue(comicDTO.getIssue());
		comic.setOriginalTitle(comicDTO.getOriginalTitle());
		comic.setOriginalIssue(comicDTO.getOriginalIssue());
		comic.setNumPages(comicDTO.getNumPages());
		comic.setHardcover(comicDTO.getHardcover());
		comic.setColor(comicDTO.getColor());
		comic.setPrice(comicDTO.getPrice());
		comic.setStockLevel(comicDTO.getStockLevel());

		return comic;
	}

	// Process file picture from itemDTO
	private static void processFile(ComicDTO comicDTO) {

		if (comicDTO.getTitlePage() != null) {
			MultipartFile titlePage = comicDTO.getTitlePage(); // get MultipartFile
			String name = titlePage.getOriginalFilename(); // get name of file
			BufferedImage picture = null; // declare new BufferedImage

			try {
				picture = ImageIO.read(new ByteArrayInputStream(titlePage.getBytes())); // read stream of picture bytes
			} catch (IOException e) {
				e.printStackTrace();
			}
			// set path for new picture
			File destination = new File("src/main/resources/static/pics/titlepages/" + name);
			try {
				ImageIO.write(picture, "jpg", destination); // save file at path
				logger.log(Level.INFO, "File " + name + " saved at " + destination);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * add Stock (item + quantity) to Inventory
	 */
	@Transactional
	public void updateInventory(Comic comic) {
		Inventory inventory = inventoryService.findByName("Main inventory");
		List<Comic> inventoryList = inventory.getInventoryList();
		inventoryList.add(comic);

	}

	/**
	 * Clears items from right-hand Cart menu
	 */
	@Transactional
	public void clearCartList(Long comicId, String greetUser) {
		Set<Comic> toRemove = new HashSet<>();
		Map<Comic, Integer> itemsInCart = getUserCart(greetUser);

		// Iterate through itemsInCart list
		for (Map.Entry<Comic, Integer> comic : itemsInCart.entrySet()) {
			if (comic.getKey().getId().equals(comicId)) { // if sent Id is contained in itemsInCart list
				toRemove.add(comic.getKey()); // add in list to be removed
			}
		}
		itemsInCart.keySet().removeAll(toRemove); // remove selected items
	}

	/**
	 * Finds the name of currently logged on user.
	 */
	public String findLoggedUserName(HttpSession session) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			String username = ((UserDetails) principal).getUsername();
			return username;
		} else {
			String username = "SESSION=" + session.getId();

			return username;
		}
	}

	/**
	 * Find a List for cart from each User.
	 */
	public Map<Comic, Integer> getUserCart(String greetUser) {
		Map<Comic, Integer> itemsInCart = new HashMap<>();

		User user = userService.findByName(greetUser);
		if (user != null) {
			itemsInCart = user.getCart(); // get user's saved cart
			return itemsInCart;
		}
		return itemsInCart;
	}

	public Double calculateTotal(ComicInCart comicInCart, String greetUser) {
		Map<Comic, Integer> itemsInCart = getUserCart(greetUser);
		Integer quantity = null;
		Double price = null;
		Double total = 0.00;
		// Calculate Total of Items in cart
		for (Entry<Comic, Integer> itemincart : itemsInCart.entrySet()) {
			Optional<Comic> findById = itemService.findById(itemincart.getKey().getId());
			Comic comic = findById.get();

			comicInCart.setItem(comic);
			comicInCart.setQuantity(itemincart.getValue());
			price = comicInCart.getItem().getPrice();
			quantity = comicInCart.getQuantity();
			total = (price * quantity) + total;

		}
		return total;
	}

	public void saveAnonimUser(String greetUser) {
		User anonimUser = new User();

		Map<Comic, Integer> itemsInCart = new HashMap<>();
		anonimUser.setCart(itemsInCart);
		anonimUser.setName(greetUser);
		itemsInCart = anonimUser.getCart();
		anonimUser.setCreationTime(LocalDateTime.now());
		userService.save(anonimUser);
		logger.log(Level.INFO, "User " + anonimUser.getName() + " saved");

	}

}
